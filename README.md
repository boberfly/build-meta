# carbonOS

carbonOS is a Linux distribution designed from the ground-up to be robust,
modern, easy-to-use, and compatible with a wide range of software. Read more
[here](https://carbon.sh)

## Project structure

- `elements/`: [BuildStream](https://buildstream.build) elements that define the OS's components
    - `pkgs/`: Defines all of the individual packages that make up the OS
    - `bootstrap/`: The basic toolchain used to bootstrap the rest of the OS
    - `buildsystems/`: Convenient way to import all the dependencies for a standard build system (meson, autotools, cmake, etc)
    - `groups/`: The basic package groups that carbonOS is split into. Everything that is pulled into an image is in a group
    - `images/`: Target device image definitions
    - `kernels/`: Variants of the Linux kernel supported by carbonOS
    - `variants/`: Variants of the base OS
    - `tools/`: Elements that are there to help the build in some way and shouldn't be treated normally
- `files/`: Various auxiliary files that are part of carbonOS's build (i.e. default config)
- `include/`: YAML fragments that are reused in `elements/` and `project.conf`
- `keys/`: Standard location to put GPG keys for signing carbonOS. Ignored by git. See `keys/README.md` for more info
- `patches/`: Patches that get applied onto packages in `elements/pkgs/`. Ideally kept to a minimum
- `plugins/`: Custom BuildStream plugins that are used in `elements/`
    - `ostree*`: A buildstream plugin that exports files into an OSTree Repository. Written for [gnome-build-meta](https://gitlab.gnome.org/GNOME/gnome-build-meta/-/tree/master/plugins)
- `project.conf`: The BuildStream project configuration
- `*.refs`: Used by BuildStream to keep track of the versions of various components
- `pub/`: Staging directory for publishing. Ignored by git
- `result/`: Standard location that `tools/checkout` exports BuildStream artifacts to
- `tools/`: Helpful scripts for building carbonOS
    - `bumpver`: Increments carbonOS's version number
    - `checkout`: Retrieves compiled artifacts from BuildStream and puts them into `result/`
    - `update`: Comprehensive script that updates every single package in the repo. RUN WITH CARE
    - `test`: Simple way to stage a few elements together and run them in a shell
    - `setup-devrepo`: Create the structure in pub/ for a development OSTree repo
    - `publish`: Publish os/base/x86_64/desktop and os/kernel/x86_64/generic to the dev repo in pub

## Build instructions

#### Dependencies

First, you need to install build dependencies. If you are running carbonOS, this
is simple:
```bash
$ updatectl switch --base=devel
[Enter administrator password at the prompt]
[Reboot]
```

Your system should now be set up for carbonOS and GDE development. If
you are not running carbonOS, you'll need to install these packages to
compile the system:

- buildstream 1.6.x
- ostree (recent)
- cargo (recent)
- go 1.14+

#### Deciding on your target

carbonOS is built somewhat modularly, and you need to decide which modules you
want to build. To do so, you'll need to answer a few questions:

> What device are you targeting? (`IMAGE`)

Device image definitions live in `elements/images/`, and they include device-specific
packages and configuration. Every image type in that folder has a README file that
will give you a good idea what devices that image is appropriate for, as well as
information that will help you answer the next few questions.

> What kernel do you want to use? (`KERNEL`)

Kernel definitions live in `elements/kernels`. Refer to your target device's README
file for guidance on what kernel to pick

> What architecture are you targetting? (`ARCH`)

This really depends on your needs. Your target device's README file will list which
CPU architectures are supported by that image.

> What variant of carbonOS do you want to use? (`VARIANT`)

carbonOS has a few basic variants that can be selected depending on your needs (desktop, devel, mobile).
They differ in available packages and in configuration.

#### Building

Now you can start by building the core OS. The core makes up most of the system;
it is only missing device-specific packages, a kernel, and an initramfs. To build the core, run:

```bash
$ bst build variants/VARIANT/files.bst
[...]
```

This will take a few hours, or longer depending on your hardware. Next, you'll
need to build a kernel+initramfs package. This makes the core you just built
bootable. Simply run:

```bash
$ bst build --max-jobs N kernels/KERNEL/all.bst
[...]
```

Replace N with the number of CPU threads you want to allocate to this build.
Since it is only building a kernel, it is better to allocate all of the CPU to
one package instead of letting multiple packages build at once as we did with
the core. To find out how many threads your CPU has available, run `nproc`.

Next, you'll build a release image of the OS that you can flash onto an install
medium or directly onto the device, depending on your needs (and on the device's
properties. Please see `elements/images/DEVICE/README.md` for details about the
generated files and where they should go). Simply run:

```bash
$ bst build images/DEVICE/image.bst
[...]
$ tools/checkout images/DEVICE/image.bst
[...]
$ ls result/
carbonOS-2020.5-DEVICE.img
```

#### Publishing

[TODO: Finish this with uploading the files to an ostree remote]

[TODO: Figure out how to allow the builder to configure where their remote is]

#### Building everything

If you are redistributing carbonOS and want to build all available cores,
kernels, and images, you can do so with this command:

```bash
$ bst build tools/everything.bst
```

Then, you can publish these binaries with:

```bash
TODO
```
