kind: script

build-depends:
- variants/desktop/repo.bst
- kernels/mainline/repo.bst

- buildsystems/base.bst
- pkgs/ostree.bst
- pkgs/genimage.bst

variables:
  baseref: os/base/x86_64/desktop
  kernelref: os/kernel/x86_64/mainline
  kargs: >-
    --karg=bgrt_disable
    --karg=quiet
    --karg=splash
    --karg=carbon.liveos
    --karg=carbon.greeter-terminal
    --karg=root=live:LABEL=%{liveos-boot-label}
    --karg=rd.live.overlay.overlayfs=1

config:
  layout:
  # Tools
  - element: buildsystems/base.bst
    destination: /
  - element: pkgs/ostree.bst
    destination: /
  - element: pkgs/genimage.bst
    destination: /
  # OS contents
  - element: variants/desktop/repo.bst
    destination: /contents/base
  - element: kernels/mainline/repo.bst
    destination: /contents/kernel
  # genimage, ostree, and sd-boot don't like buildstream's default fuse magic
  - element: ''
    destination: /build
  - element: ''
    destination: /sysroot
  - element: ''
    destination: /bootloader

  commands:
  - | # Set up OSTree
    ostree admin init-fs --modern /sysroot
    ostree admin os-init --sysroot=/sysroot carbonOS
    ostree config --repo=/sysroot/ostree/repo set sysroot.bootloader none

  - | # Copy files
    ostree pull-local --repo=/sysroot/ostree/repo /contents/base %{baseref}
    ostree pull-local --repo=/sysroot/ostree/repo /contents/kernel %{kernelref}

  - | # Merge the commits
    ostree commit --repo=/sysroot/ostree/repo --base=%{baseref} --tree=ref=%{kernelref} -b os/x86_64 \
      --bootable --add-metadata-string=version=%{version} \
      --add-metadata-string=carbon.base-csum=$(ostree --repo=/sysroot/ostree/repo rev-parse %{baseref}) \
      --add-metadata-string=carbon.kernel-csum=$(ostree --repo=/sysroot/ostree/repo rev-parse %{kernelref})

  - | # Deploy
    cat >> /tmp/origin <<EOF
    [origin]
    BaseVariant=desktop
    KernelVariant=mainline
    Collection=sh.carbon.Stable
    EOF
    ostree admin deploy --sysroot=/sysroot --os=carbonOS %{kargs} --origin-file=/tmp/origin os/x86_64

  - | # Make dracut recognize the squashfs as an OS
    mkdir /sysroot/proc

  - | # Install systemd-boot
    SYSTEMD_RELAX_ESP_CHECKS=1 bootctl install --no-variables --esp-path=/bootloader
    rm -r /bootloader/loader/{entries,loader.conf}
    cp -ar /bootloader/* /sysroot/boot

  - | # Configure systemd-boot
    sed -i 's/title.*/title Try or Install carbonOS/' /sysroot/boot/loader/entries/ostree-1-carbonOS.conf
    cp /sysroot/boot/loader/entries/{ostree-1-carbonOS,z-carbonOS-ramdisk}.conf
    sed -i 's/title.*/& (RAM Disk)/' /sysroot/boot/loader/entries/z-carbonOS-ramdisk.conf
    sed -i 's/options.*/& rd.live.ram/' /sysroot/boot/loader/entries/z-carbonOS-ramdisk.conf
    cat > /sysroot/boot/loader/loader.conf <<EOF
    timeout 5
    editor yes
    auto-entries no
    auto-firmware yes
    default ostree-1-carbonOS.conf
    EOF

  - | # Create the working directories
    mkdir -pv /build/{stage1,stage2,isoroot/LiveOS,images}

  - | # genimage consumes /sysroot/boot, but we need it to be populated for ostree to work
    cp -ar /sysroot/boot /sysroot/liveboot

  - | # Create the genimage configs
    get_size() {
      size=$(($(du --apparent-size -B 512 -s "${1}" | cut -f1)/2))
      echo $((${size}+(${size}/10)))k
    }

    cat >/build/stage1/genimage.cfg <<EOF
    image squashfs.img {
    	mountpoint = "/"
    	squashfs {
    	    compression = "zstd"
    	}
    }
    image boot.img {
        mountpoint = "/liveboot"
        vfat {
        	extraargs="-F 32"
        	label = "EFI"
        }
        size = $( get_size /sysroot/liveboot )
    }
    config {
        rootpath = "/sysroot"
        inputpath = "/build/images"
        outputpath = "/build/images"
    }
    EOF

    cat >/build/stage2/genimage.cfg <<EOF
    image installer.iso {
        iso {
            extraargs = "-e /boot.img -no-emul-boot -boot-load-size 4 -efi-boot-part --efi-boot-image -sysid LINUX -publisher carbonOS"
            volume-id = "%{liveos-boot-label}"
        }
    }
    config {
        rootpath = "/build/isoroot"
        inputpath = "/build/images"
        outputpath = "/build/images"
        genisoimage = "xorrisofs"
    }
    EOF

  - | # Build the squashfs & ESP images
    cd /build/stage1
    genimage

  - | # Put the generated files in the proper place
    cd /build/images
    mv squashfs.img /build/isoroot/LiveOS/squashfs.img
    mv boot.img /build/isoroot/boot.img

  - | # Build the ISO
    cd /build/stage2
    genimage

  - | # Copy the output
    mv /build/images/installer.iso %{install-root}/carbonOS-%{version}-installer.iso
