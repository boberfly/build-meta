kind: autotools

sources:
- kind: git_tag
  url: fdo-git:polkit/polkit
- kind: local
  path: files/polkit
  directory: carbon

# TODO: Remove once this commit lands in a tag
# https://gitlab.freedesktop.org/polkit/polkit/-/commit/a2bf5c9c83b6ae46cbd5c779d3055bff81ded683
- kind: patch
  path: patches/polkit/CVE-2021-4034.patch

# TODO: Remove once this commit lands in a tag
# https://gitlab.freedesktop.org/polkit/polkit/-/commit/a6bedfd09b7bba753de7a107dc471da0db801858
- kind: patch
  path: patches/polkit/mozjs-91.patch

# TODO: Port to meson

depends:
- pkgs/glib.bst
- pkgs/mozjs.bst
- pkgs/expat.bst
- pkgs/pam.bst
- pkgs/systemd/all.bst

build-depends:
- pkgs/intltool.bst
- pkgs/gobject-introspection.bst
- pkgs/gtk-doc.bst
- buildsystems/autotools.bst

variables:
  conf-local: >-
    --disable-test
    --enable-libsystemd-login=yes
    --with-authfw=pam
    --disable-gtk-doc
    --disable-man-pages
  make-args: dbusconfdir=%{datadir}/dbus-1/system.d rulesdir=/usr/share/polkit-1/rules.d

config:
  install-commands:
    (>):
    - | # Install sysusers.d config
      install -Dvm644 carbon/sysusers.conf %{install-root}%{libdir}/sysusers.d/polkit.conf
    - | # Move PAM config. TODO: Once meson port lands, use that instead
      mv %{install-root}%{sysconfdir}/pam.d %{install-root}%{libdir}/pam.d
    - | # Install pkexec config
      install -Dvm644 carbon/pkexec.rules %{install-root}%{datadir}/polkit-1/rules.d/60-pkexec.rules
