kind: meson
description: |
  This package allows carbonOS to have H.264 codec support

  Since H.264 is patent-encumbered, packaging openh264 or x264 would require
  that I pay royalties to MPEG-LA. Cisco provide a pre-compiled binary of openh264
  for which they agree to cover the cost of the royalties, but I am not allowed
  to redistribute it. Instead, the noopenh264 package allows me to link
  against the openh264 library and then download the Cisco binary at runtime.

  This runtime download is handled via Flatpak.

  Ideally, we'd rely on VAAPI and use that to decode H.264 video on carbonOS. However,
  not all GPUs have built-in decoders so a software implementation is necessary.

sources:
- kind: git
  url: github:endlessm/noopenh264
- kind: remote
  url: http://www.openh264.org/BINARY_LICENSE.txt
- kind: local
  path: files/noopenh264
  directory: carbon

build-depends:
- buildsystems/meson.bst

variables:
  license-files: COPYING* BINARY_LICENSE.txt

config:
  install-commands:
    (>):
    - |
      cd carbon
      install -Dm644 ldconfig.conf %{install-root}%{sysconfdir}/ld.so.conf.d/openh264.conf
      install -Dm644 -t %{install-root}%{datadir}/flatpak org.freedesktop.Platform.openh264.flatpakref
