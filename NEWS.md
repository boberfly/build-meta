# carbonOS 2022.1 (????)

- Linux 5.??.?? kernel
- GNOME 42 platform
- Latest security fixes
- Graphite Desktop Environment Updates:
    - Official dark-mode support!
    - Implemented OSD popup to visually indicate volume and brightness adjustments
    - Upgrade many of the built-in apps to use GTK4 and libadwaita
    - Restyle GDE shell elements to fit in with new libadwaita theme
    - New included apps: Disk Usage Analyzer
- System update manager updates:
    - `updatectl rollback` command to easily uninstall system updates
    - Integration with GNOME Software
- Added podman, to provide a familiar docker-like environment for developers
- Added support for thunderbold devices
- Added rtkit daemon, which allows select user services to give themselves realtime scheduling
- Drop doas; all privilage escalation in carbonOS is now managed through polkit and pkexec
- Curated the list of built-in codecs (See issue #20)

# carbonOS 2021.1 (Alpha)

- Complete source-code restructuring
- Enable many different build-time security options and speed optimizations
- Remove debugging information from the binaries, making the OS much smaller
- Implement nsbox command environment
- Settings app
- Switch carbonOS to btrfs
- Enable filesystem compression (reduce size of logs, cache, and the OS; increase drive longevity)
- Gracefully handle system OOM: swap-on-zram and systemd-oomd
- Replace sudo with doas
- Replace pulseaudio with pipewire
- Update boot animation to show carbonOS logo if no firmware logo is available
- Implement system updates service and CLI tool
- New Graphite login screen
- New graphical installer and initial setup app
- Bug fixes all throughout the system
- Many, many more changes! This was a huge release!

# carbonOS 2020.1 (Pre-alpha)

- Initial release!
