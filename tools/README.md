# Tools

Contains various helper scripts that make building and installing carbonOS easier.

Scripts:
- `tools/bumpver`: Bumps up carbonOS's version number. Do this after a release!
- `tools/checkout`: Checkout out artifacts out of Buildstream and into result/
- `tools/kconf`: Reconfigures the kernel (99% CHANCE IT'S BROKEN!!!!)
- `tools/update`: Updates all the packages in the OS
- `tools/test`: Takes a list of Buildstream elements as args. Stages all these arguments together and drops into shell
- `tools/manual-update`: Updates packages that require manual editing of the build file. Part of `tools/update`
- `tools/setup-devrepo`: Creates the development repo @ pub/repo
- `tools/publish`: Publishes carbonOS to the dev repo @ pub/repo
